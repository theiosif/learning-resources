# **CTF-related collection of information**
## **Cryptosports galore and much more**

#### **NOTE:**
This is by no means a be-all, end-all document. Its structure and contents may get more polished as time passes.   
If anything, consider it a "library" of sorts.   
Please refer to [this absolutely amazing spreadsheet](https://docs.google.com/spreadsheets/d/12bT8APhWsL-P8mBtWCYu4MLftwG1cPmIL25AEBtXDno/edit#gid=937533738) for more learning material. Links hosted there will soon be added in this repo.

### **Challenges**
**_a.k.a. your new daily activity_**   
**These are basically puzzles based on the 'learn by doing' mentality.** _Most have solutions posted online (only use this as a last resort)._   
_Bookmark one link and return for another when you're done._  

* Categorised collection of challenge websites
    * http://captf.com/practice-ctf/
* Other challenge websites (entry-level ones listed first):
    * http://www.wechall.net  <- _also has links to other sites_  
    * [overthewire.org/wargames](http://overthewire.org/wargames/bandit/bandit0.html)  <--- **strong starting point** for knowing Linux basics. [Get PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) if you're on Windows.   
    * [exploit-education](https://exploit.education/), formerly _exploit-exercises.com_. VM-based learning, everything is preinstalled. Very good starting point.   
    * http://www.hackthissite.org <--- another good starting point   
    * http://www.rankk.org <--- yet another good starting point   
    * [ctf101.org](https://ctf101.org) <--- name says it all   
    * https://www.net-force.nl/challenges/  
    * http://www.hellboundhackers.org   
    * http://www.thisislegal.com     
    * http://www.enigmagroup.org  
    * https://www.hackthebox.eu  
    * [+Ma's Reversing - main page](http://www.3564020356.org/)  
    * [[Deutsch\] happy-security.de](http://www.happy-security.de/)  
    * http://www.bright-shadows.net - offline at the time of writing (goddamnit GDPR)   
    * http://www.lost-chall.org <-- for LOST fans   
    * http://www.hackquest.org   
    * https://microcorruption.com/login <-- embedded stuff  
    * https://www.root-me.org/?lang=en

### **Other guides/courses**
* [VulnHub resources](https://www.vulnhub.com/resources/) <-- wish I found this sooner, please check it out.
* [!gh Awesome-Hacking](https://github.com/Hack-with-Github/Awesome-Hacking/blob/master/README.md)
* [TrailOfBits CTF field guide](https://trailofbits.github.io/ctf/)
* [mayfrost's repo](https://github.com/mayfrost/guides/) - Select collection of guides
* [[Course ACS-Poli\] Computer Network Security](https://ocw.cs.pub.ro/courses/cns)
* [[CS.FSU-Course \] Offensive Security](https://www.cs.fsu.edu/~redwood/OffensiveSecurity/lectures.html)
* [primalsecurity.com \[Python & Exploit tutorials\]](http://www.primalsecurity.net/tutorials/)

### **Writeups**
* [medium.com writeups](https://medium.com/bugbountywriteup/tagged/ctf)
* [CTFTime.org writeups](https://ctftime.org/writeups)
* [Corb3nik's writeups](https://corb3nik.github.io/)

### **YouTube Channels / Video Material**
_I learned photoshop by watching sped-up photomanipulation videos. Consider these a sort of "apprenticeship" looking-over-the-master's-shoulder sort of thing._    


* __Youtube__
    * [Murmus](https://www.youtube.com/MurmusCTF)
    * [LiveOverflow](https://www.youtube.com/LiveOverflowCTF)
    * [John Hammond](https://www.youtube.com/user/RootOfTheNull)
    * [IppSec](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA)    
* __Other video sources__
    * [SecurityTube](http://www.securitytube.net/)
    * [IronGeek](https://www.irongeek.com/)


### **Linux-related**
**Pretty please**: If you still don't have Linux, or you're running it inside a VM, please _'waste'_ a couple of hours and follow this guide, to avoid wasting a lot of time down the line due to unbelievable amounts of VM shenanigans: [Dualbooting W10 and Ubuntu](https://pureinfotech.com/dual-boot-ubuntu-windows-10/)    

* [bash cheatsheet](https://devhints.io/bash)
* [info coreutils](https://www.manpagez.com/info/coreutils/)   - everybody should read these at least once
* [Linux from scratch](http://www.linuxfromscratch.org/) - build your own linux system
* **Bookz**:  
    * **More theoretical**:  
        * Modern Operating Systems by Andrew S. Tanenbaum   
    * **More 'just do it'**:   
        * How linux Works, Brian Ward     
        * The linux command line, W. Shotts [[LINK\]](http://www.linuxcommand.org/tlcl.php/)   

### **Reverse**
* [The Art of Assembly Language](http://faculty.petra.ac.id/indi/files/The%20Art%20of%20Assembly%20Language%2032-bit%20Edition.pdf)
* [Reverse Engineering for beginners](https://beginners.re/)
* [The IDA Pro Book](http://www.allitebooks.com/the-ida-pro-book/)
    * [Link to binaries in the book](http://idabook.com/examples/index.html)

### **Forensics**
* [ForensicsWiki](forensicswiki.org/wiki/Main_Page)


### **CTF Toolkits**
* [zardus's repo](https://github.com/zardus/ctf-tools)

### **Misc.**   
* [Hash Length Extension Attack tool](https://github.com/bwall/HashPump)
* [Default password collection](https://cirt.net/passwords)  
* [Wordlists collection](https://wiki.skullsecurity.org/Passwords)
* [Cryptoparty handbook](https://www.cryptoparty.in/learn/handbook) - Online privacy tools primer    
* [Linux hardening guide](https://github.com/trimstray/the-practical-linux-hardening-guide)
* [!gh awesome-ctf](https://github.com/apsdehal/awesome-ctf)
* [This pastebin](https://pastebin.com/raw/0SNSvyjJ)

* **Bookz**:  
    * Clean Code, Robert C. Martin   
    * Steal this computer book, W. Wang
