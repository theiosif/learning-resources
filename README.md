# Learning materials
### ** Ever-expanding collection of various resources, related to my research interests.**
---
## **$ whoami**
[LinkedIn](https://www.linkedin.com/in/theiosif/)
[Facebook](https://www.facebook.com/TheIosif)
[Twitter](https://twitter.com/the_iosif)
[Instagram](https://www.instagram.com/the.iosif/)   
Master's student in Communications Engineering at TUM. BSc in Electronics & CS at Politehnica Bucharest.    
Eternally curious, might break and fix stuff in the process of learning.  

[Q]: *Why include this?*   
[A]: *Networking has already permeated every aspect of our lives, might as well make it work for me.*

---
## **$ls**
* [CTF-related materials](https://bitbucket.org/theiosif/learning-resources/src/master/CTF-related.md)     
* [Networking 101 + Cisco IOS](https://bitbucket.org/theiosif/ccna-1-2-labwork-summaries/src/master/)
